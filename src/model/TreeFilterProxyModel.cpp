/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * Filter function are from :
 * https://qt-project.org/forums/viewthread/7782/#45740
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TreeFilterProxyModel.hpp"
#include "IconListModel.hpp"
#include <QDebug>
#include <QSettings>
#include <QCoreApplication> 

/*!
 * 
 */
TreeFilterProxyModel::TreeFilterProxyModel(QObject* parent) :
    QSortFilterProxyModel( parent ) , 
    m_showExtra(false) ,
    m_showUnset(false)
{
    setFilterRole( Qt::DisplayRole );
    setFilterCaseSensitivity( Qt::CaseInsensitive );
    connect( this , SIGNAL(searchStringChanged(QString)) , SLOT(setFilterWildcard(QString)) );
    readSettings();
}

/*!
 * 
 */
TreeFilterProxyModel::~TreeFilterProxyModel()
{
    writeSettings(); 
}


/*!
 * 
 */
bool TreeFilterProxyModel::showExtra() const
{
   return m_showExtra ;
}

/*!
 * 
 */
void TreeFilterProxyModel::setShowExtra(bool show)
{
   m_showExtra = show ;
   emit searchStringChanged( searchString() );
}

/*!
 * 
 */
bool TreeFilterProxyModel::showUnset() const
{
   return m_showUnset ;   
}

/*!
 * 
 */
void TreeFilterProxyModel::setShowUnset(bool show)
{
   m_showUnset = show ;
   emit searchStringChanged( searchString() );
}

/*!
 * 
 */
QString TreeFilterProxyModel::searchString() const
{
    return m_searchString ;
}

/*!
 * 
 */
void TreeFilterProxyModel::setSearchString(const QString & str)
{
    m_searchString = str ;
    emit searchStringChanged(str);
}

/*!
 * 
 */
bool TreeFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex & source_parent) const
{
   if ( hasAcceptedChildren(source_row,source_parent) )
   {
      return true ;
   }
   else 
      return filterAcceptsRowItself(source_row,source_parent) ;
   
}
 
/*!
 * 
 */
bool TreeFilterProxyModel::filterAcceptsRowItself(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex item = sourceModel()->index(source_row,0,source_parent) ;

    // remove line if icon is unset
    if ( item.data(Qt::UserRole) == IconListModel::Icon && !showUnset() )
    {
        QIcon icon = item.data(Qt::DecorationRole).value<QIcon>() ;
        if ( icon.isNull() )
            return false ;
    }
    
    // Remove line if name is extra
    if ( item.data(Qt::UserRole) == IconListModel::Category && !showExtra() )
    {
	QString name = item.data(Qt::DisplayRole).toString().toLower() ;
	if ( name.contains( "extra" ) )
	  return false ;
    }

    // remove line if parent contains extra
    if ( ! showExtra() )
    {
        QModelIndex parent = item.parent() ;
        while ( parent.isValid() )
        {
            if ( parent.data(Qt::UserRole) == IconListModel::Category )
            {
                QString name = parent.data(Qt::DisplayRole).toString().toLower() ;
                if ( name.contains( "extra" ) )
                    return false ;
            }
            parent = parent.parent() ;
        }
    }

    return QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent) ;
}
 
 
/*!
 * 
 */
bool TreeFilterProxyModel::hasAcceptedChildren(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex item = sourceModel()->index(source_row,0,source_parent);
    if (!item.isValid()) 
    {
        return false;
    }
 
    //check if there are children
    int childCount = item.model()->rowCount(item);
    if (childCount == 0)
        return false;
 
    for (int i = 0; i < childCount; ++i) 
    {
        if (filterAcceptsRow(i, item))
            return true;
    }
 
    return false;
}

/*!
 * 
 */
int TreeFilterProxyModel::iconsCount() const
{
    int count = 0 ;

    for (int r = 0 ; r < rowCount() ; r++)
    {
        const QModelIndex idx = index(r,0) ;

        if (idx.isValid())
            count += rowCount(idx) ;
    }

    return count ;
}

/*!
 * Read persitent settings of filter
 */
void TreeFilterProxyModel::readSettings()
{
    QSettings settings( qAppName() , qAppName() ) ;
#ifdef Q_OS_LINUX
    setShowExtra( settings.value("filter/showExtra",false).toBool() );
    setShowUnset( settings.value("filter/showUnset",true).toBool() );
#else
    setShowExtra( settings.value("filter/showExtra",false).toBool() );
    setShowUnset( settings.value("filter/showUnset",false).toBool() );
#endif
}

/*!
 * Write persitent settings
 */
void TreeFilterProxyModel::writeSettings() const
{
   QSettings settings( qAppName() , qAppName() ) ;
   settings.setValue( "filter/showExtra" , showExtra() ); 
   settings.setValue( "filter/showUnset" , showUnset() ); 
}

