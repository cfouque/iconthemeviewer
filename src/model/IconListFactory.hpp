/*
 * 
 * Copyright (C) 2014  Clement Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __ICONLISTFACTORY_HPP__
#define __ICONLISTFACTORY_HPP__

#include <QObject>
#include <QDomElement>
#include <QStyle>
#include "IconListModel.hpp"

class IconListFactory : public QObject
{
    Q_OBJECT
    
    IconListModel * m_iconList ;
    QMap<QString,QString> m_fancyCategoryNames ;
    QMap<QString,QStringList>  m_standardNamesByCats ;

public:
    explicit IconListFactory( QObject * parent = 0 );
    ~IconListFactory();
    IconListModel * model() const ;

public slots :
   void createModel( QObject * parent = 0 ) ;
   void populateModelFromFile( const QString& filename ) ;
   void addQtStandardPixmaps() const ;
   void fetchExtraIcon() const ;
   
private :
    QStringList getEntriesFromTable(const QDomElement & tableElt , const int & col = 0 ) const ;
    QString reformatString( const QString & str ) const ;
    QStandardItem * createCategoryItem( const QString & name , const QString & desc = QString() ) const ;
    QStandardItem * createIconItem( const QString & name , const QIcon & icon = QIcon() , const QString & desc = QString() ) const ;
    bool isStandardIcon( const QString & name ) const ;
};

#endif
