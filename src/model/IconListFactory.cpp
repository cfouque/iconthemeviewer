/*
 *
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IconListFactory.hpp"
#include "ThemeSelector.hpp"
#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QStyle>
#include <QStandardItemModel>
#include <QDomElement>
#include <QDomDocument>
#include <QMetaEnum>
#include <QDir>
#include <qsettings.h>


/*!
 * Constructor
 */
IconListFactory::IconListFactory( QObject * parent ) :
    QObject(parent) ,
    m_iconList(0)
{

}

/*!
 * Destructor
 */
IconListFactory::~IconListFactory()
{

}

/*!
 * Initialize the model
 */
void IconListFactory::createModel(QObject* parent)
{
    if ( !m_iconList )
    {
        if ( !parent ) parent = this ;
        m_iconList = new IconListModel(parent) ;
    }
}

/*!
 * Accessor to the model
 */
IconListModel* IconListFactory::model() const
{
    return m_iconList ;
}

/*!
 * Read specs from file
 * http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.xml
 */
void IconListFactory::populateModelFromFile(const QString & filename)
{
    QFile file(filename) ;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qCritical() << "Unable to open" << filename ;
        return ;
    }

    QDomDocument doc ;
    doc.setContent(&file) ;

    QMap< QString , QStandardItem *> catItems ;
    QDomElement root = doc.firstChildElement("article") ;

    for (QDomElement elt = root.firstChildElement("sect1") ; !elt.isNull() ; elt = elt.nextSiblingElement("sect1"))
    {
        // Get categories
        if (elt.attribute("id") == "context")
        {

            QDomElement tableElt = elt.firstChildElement("table") ;
            QStringList categories = getEntriesFromTable(tableElt , 0) ;
            QStringList description = getEntriesFromTable(tableElt , 1) ;
            QStringList ids = getEntriesFromTable(tableElt , 2) ;

            for (int n = 0 ; n < categories.size() ; n++)
            {
                QStandardItem * item = createCategoryItem( categories.at(n) , description.at(n) ) ;
		model()->invisibleRootItem()->appendRow( item );
                catItems.insert(ids.at(n) , item) ;
		m_fancyCategoryNames.insert( ids.at(n) , categories.at(n) ) ;
            }

        }
        else if (elt.attribute("id") == "names")
        {

            // Loop thru table found in name section
            for (QDomElement tableElt = elt.firstChildElement("table") ; !tableElt.isNull() ; tableElt = tableElt.nextSiblingElement("table"))
            {
                const QString tableId = tableElt.attribute("id") ;

                // Fisrt find the correct category item
                QStandardItem * catItem = catItems.value(tableId) ;

                // if table is found, read it
                if (!catItem) continue ;

                QStringList names = getEntriesFromTable(tableElt , 0) ;
                QStringList desc = getEntriesFromTable(tableElt , 1) ;

                for (int n = 0 ; n < names.size() ; n++)
                {
                    QStandardItem * nItem = createIconItem( names.at(n) , QIcon::fromTheme( names.at(n) ) , desc.at(n) ) ;
                    catItem->appendRow(nItem);
                }
                
                m_standardNamesByCats.insert( tableId , names ) ;
            }
        }
    }
}

/*!
 *
 */
QStringList IconListFactory::getEntriesFromTable(const QDomElement & tableElt, const int & col) const
{
    QDomElement tgroupElt = tableElt.firstChildElement("tgroup") ;
    int colCount = tgroupElt.attribute("cols").toInt() ;
    Q_ASSERT(col < colCount) ;

    QStringList values ;
    QDomElement tbodyElt = tgroupElt.firstChildElement("tbody") ;

    for (QDomElement rowElt = tbodyElt.firstChildElement("row") ; !rowElt.isNull() ; rowElt = rowElt.nextSiblingElement("row"))
    {
        QDomElement entry = rowElt.elementsByTagName("entry").at(col).toElement() ;
        QString txt = reformatString( entry.text() );
        values.append(txt);
    }

    return values ;
}

/*!
 *
 */
QString IconListFactory::reformatString(const QString & str) const
{
    QString retStr = str ;
    if ( retStr.contains('\n') )
    {
        retStr.replace( QChar('\n') , QChar(' ') ) ;
    }

    retStr.replace( "." , ".\n\n" ) ;

    while( retStr.at(0) == ' '  )
    {
        retStr.remove(0,1) ;
    }
    return retStr ;
}

/*!
 * Add icons from Qt standars pixmaps :
 * http://qt-project.org/doc/qt-4.8/qstyle.html#StandardPixmap-enum
 */
void IconListFactory::addQtStandardPixmaps() const
{
    // Open xml doc
    QFile file( ":/naming-specs/qt" ) ;
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qCritical() << "Unable to open" << file.fileName() ;
        return ;
    }

    QDomDocument doc ;
    doc.setContent(&file) ;
    QDomElement rootElt = doc.firstChildElement( "enum" ) ;
    if ( rootElt.isNull() || rootElt.attribute("id") != "QStyle::StandardPixmap" ) 
    {
        qCritical() << "bad file spec" ;
        return ;
    }
    
    QString desc = rootElt.firstChildElement("desc").text() ;
    QStandardItem * category = createCategoryItem( "Qt standard icons" , desc ) ;
    model()->invisibleRootItem()->appendRow( category );
    
    QDomElement valueElt = rootElt.firstChildElement("value") ;
    while( !valueElt.isNull() )
    {
        int n = valueElt.attribute( "id" ).toInt() ;
        QStyle::StandardPixmap sp = static_cast<QStyle::StandardPixmap>(n) ;
        
        QString id = valueElt.firstChildElement( "name" ).text() ;
        QString desc = valueElt.firstChildElement( "desc" ).text() ;
        QIcon icon = QApplication::style()->standardIcon(sp) ;
        QStandardItem * item = createIconItem( id , icon , desc ) ;
        category->appendRow(item);
        valueElt = valueElt.nextSiblingElement("value") ;
    }
}

/*!
 *
 */
QStandardItem * IconListFactory::createCategoryItem(const QString & name , const QString & desc ) const
{
    QStandardItem * category = new QStandardItem( name ) ;
    category->setData( IconListModel::Category , Qt::UserRole );

    if ( !desc.isEmpty() )
        category->setData( desc , Qt::WhatsThisRole );

    return category ;
}

/*!
 * 
 */
QStandardItem * IconListFactory::createIconItem(const QString& name, const QIcon& icon, const QString& desc) const
{
    QStandardItem * item = new QStandardItem( name ) ;
    item->setData( IconListModel::Icon , Qt::UserRole );

    if ( !desc.isEmpty() )
        item->setData( desc , Qt::WhatsThisRole );
    
    if ( !icon.isNull() )
        item->setData( icon , Qt::DecorationRole );
    else
        item->setData( QColor(Qt::red) , Qt::TextColorRole );

    return item ;
}

/*!
 * 
 */
void IconListFactory::fetchExtraIcon() const
{
   // find current icons directory
   QDir themeDir = ThemeSelector::getThemeDirectory() ;
   
   // parse theme information ;
   QString fileName = QString( "index.theme" ) ;
   QFile file( themeDir.absoluteFilePath(fileName) ) ;
   QString dirLines ;
   if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
   {
       QTextStream stream(&file) ;
       while( !stream.atEnd() )
       {
	  dirLines = stream.readLine() ;
	  if ( dirLines.contains("Directories") )
	    break ;
       }
   }
   else
     qCritical() << "Unable to open" << file.fileName() << file.errorString() ;
   
    // list icons
    QMap<QString,QStringList> extraIcons ;
    dirLines.remove("Directories=") ;
    QStringList folders = dirLines.split(",") ;
    QStringList nameFilter ;
    nameFilter.append( "*.png" );
    foreach( QString folder , folders )
    {
      QString cat = folder.split("/").takeLast() ;
      
      QDir f( themeDir.absoluteFilePath( folder ) );
      QStringList icons = f.entryList( nameFilter , QDir::Files ) ;
      if ( extraIcons.contains(cat) )
      {
	 extraIcons[cat] << icons ;
	 extraIcons[cat].removeDuplicates() ;
      }
      else
      {
	 extraIcons.insert( cat , icons ) ;
      }
    }
    
    // Remove duplicates entries and standard icons 
    
    
    // Add to list 
    QStandardItem * catItem = createCategoryItem("Extra icons") ;
    model()->invisibleRootItem()->appendRow(catItem);
    foreach( QString cat , extraIcons.uniqueKeys() )
    {
	QStandardItem * subCatItem = 0 ;
        if ( m_fancyCategoryNames.contains(cat)) 
	   subCatItem = createCategoryItem( m_fancyCategoryNames[cat] ) ;
	else 
	   subCatItem = createCategoryItem( cat ) ;
	
        catItem->appendRow(subCatItem);
	
        foreach( QString iconName , extraIcons[cat] )
        {
	    iconName.remove(".png");
	    if ( ! isStandardIcon(iconName) )
	    {
	       QIcon icon = QIcon::fromTheme(iconName) ;
	       QStandardItem * i = createIconItem( iconName , icon ) ;
	       subCatItem->appendRow(i);
	    }
        }
    }
}

/*!
 * 
 */
bool IconListFactory::isStandardIcon(const QString& name) const
{
    foreach( QString k , m_standardNamesByCats.uniqueKeys() )
    {
       if ( m_standardNamesByCats[k].contains(name) )
       {
	  return true ;
       }
    }
    
    return false ;
}
