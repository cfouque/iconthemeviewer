/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * Filter function are from :
 * https://qt-project.org/forums/viewthread/7782/#45740
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __TREEFILTERPROXYMODEL_HPP__
#define __TREEFILTERPROXYMODEL_HPP__

#include <QSortFilterProxyModel>

class TreeFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY( bool showExtra READ showExtra WRITE setShowExtra ) 
    Q_PROPERTY( bool showUnset READ showUnset WRITE setShowUnset )
    Q_PROPERTY( QString searchString READ searchString WRITE setSearchString NOTIFY searchStringChanged )
    
    bool m_showExtra ;
    bool m_showUnset ;
    QString m_searchString ;

public:
    explicit TreeFilterProxyModel( QObject * parent = 0 );
    virtual ~TreeFilterProxyModel();
    int iconsCount() const ;
    bool showExtra() const ;
    bool showUnset() const ;
    QString searchString() const ;
    
public slots :
    void setShowExtra( bool show ) ;
    void setShowUnset( bool show ) ;
    void setSearchString( const QString & str ) ;
    
protected :
    virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const ;
    bool filterAcceptsRowItself(int source_row, const QModelIndex &source_parent) const ;
    bool hasAcceptedChildren(int source_row, const QModelIndex &source_parent) const ;
    void readSettings() ;
    void writeSettings() const ;
    
signals :
    void filtered() const ;
    void searchStringChanged( QString ) const ;
};

#endif