/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __ICONLISTAREA_HPP__
#define __ICONLISTAREA_HPP__

#include <QWidget>
#include <QModelIndex>

class IconListModel;
class TreeFilterProxyModel ;

namespace Ui
{
class IconListArea;
}

class IconListArea : public QWidget
{
    Q_OBJECT

    Ui::IconListArea * ui ;
    TreeFilterProxyModel * m_filterProxy ;

public:
    explicit IconListArea(QWidget * parent = 0) ;
    ~IconListArea() ;

public slots :
    void onItemActivation(const QModelIndex & index) ;
    void onCopyToClipboard() ;
    void setModel(IconListModel * model) ;
    void updateStatusTip() ;
    void refreshView() ;

private :
    void createActions() ;
    
signals :
    void itemActivated(const QModelIndex & index) ;
    void statusUpdated( const QString & msg ) ;
};

#endif // ICONLISTAREA_H
