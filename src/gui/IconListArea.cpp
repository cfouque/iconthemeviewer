/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IconListArea.hpp"
#include "ui_IconListArea.h"

#include "TreeFilterProxyModel.hpp"
#include "IconListModel.hpp"

#include <QClipboard>
#include <QDebug>
#include <QMenu>

/*!
 * Constructor
 */
IconListArea::IconListArea(QWidget * parent) :
    QWidget(parent)
{
    ui = new Ui::IconListArea;
    ui->setupUi(this);
    
    // Add icon to empty label
    QIcon searchIcon = QIcon::fromTheme("edit-find" , QIcon(":/icons/Search-icon.png")) ;
    QSize s = ui->label->size() ;
    ui->label->setPixmap( searchIcon.pixmap(s) );

    // Icon for Menu with cross-plateform fallback
    QIcon prefIcon = QIcon::fromTheme( "preferences-other" , QApplication::style()->standardIcon(QStyle::SP_FileDialogContentsView) ) ;
    ui->configButton->setIcon( prefIcon );

    // Create the filter proxy
    m_filterProxy = new TreeFilterProxyModel(this) ;
    ui->treeView->setModel(m_filterProxy);
    connect( ui->searchField , SIGNAL(textChanged(QString)) , m_filterProxy , SLOT(setSearchString(QString))) ;
    connect( ui->treeView->selectionModel() , SIGNAL(currentChanged(QModelIndex,QModelIndex)) , SLOT(onItemActivation(QModelIndex))) ;
    
    // Straing validator for search field
    QRegExpValidator * searchValidator = new QRegExpValidator( ui->searchField ) ;
    ui->searchField->setValidator( searchValidator );
    searchValidator->setRegExp( QRegExp("\\S+") );
    connect( ui->searchField , SIGNAL(returnPressed()) , ui->treeView , SLOT(setFocus()) ) ;
    
    // Creation of option menu
    ui->configButton->setMenu( new QMenu(this) ) ;

    QAction * showExtra = ui->configButton->menu()->addAction( tr("Show extra icons") , m_filterProxy , SLOT(setShowExtra(bool)) ) ;
#ifdef Q_OS_LINUX
    showExtra->setCheckable(true);
    showExtra->setChecked( m_filterProxy->showExtra() );
#else
    showExtra->setEnabled(false);
#endif

    QAction * showUnset = ui->configButton->menu()->addAction( tr("Show unset icons") , m_filterProxy , SLOT(setShowUnset(bool)) ) ;
#ifdef Q_OS_LINUX
    showUnset->setCheckable(true);
    showUnset->setChecked( m_filterProxy->showUnset() );
#else
    showUnset->setEnabled(false);
#endif
    
    createActions();
}

/*!
 * Destructor
 */
IconListArea::~IconListArea()
{
    delete ui;
}

/*!
 * 
 */
void IconListArea::createActions()
{
    QAction * clipboardAction = new QAction( QIcon::fromTheme("edit-copy") , tr("Copy to clipboard") , this ) ;
    connect( clipboardAction , SIGNAL(triggered(bool)) , SLOT(onCopyToClipboard()) ) ;
    ui->treeView->addAction( clipboardAction );
    clipboardAction->setObjectName( "clipboardAction" );
    clipboardAction->setStatusTip( tr("Copy icon code to clipboard.") );
    clipboardAction->setShortcut( QKeySequence::Copy );
    
    QAction * separator = new QAction(this) ;
    separator->setSeparator(true) ;
    ui->treeView->addAction( separator );
    
    QAction * expandAction = new QAction( tr("Expand all") , this ) ;
    connect( expandAction , SIGNAL(triggered(bool)) , ui->treeView , SLOT(expandAll()) ) ;
    ui->treeView->addAction( expandAction );
    expandAction->setShortcut( QKeySequence::ZoomIn );
    expandAction->setStatusTip( tr("Expand all categories.") );
    
    QAction * collapseAll = new QAction( tr("Collapse all") , this ) ;
    connect( collapseAll , SIGNAL(triggered(bool)) , ui->treeView , SLOT(collapseAll()) ) ;
    ui->treeView->addAction( collapseAll );
    collapseAll->setShortcut( QKeySequence::ZoomOut );
    collapseAll->setStatusTip( tr("Collapse all categories.") );
}


/*!
 * Sets the model to the filter proxy
 */
void IconListArea::setModel(IconListModel * model)
{
    m_filterProxy->setSourceModel(model) ;
    if ( model->rowCount() == 1 )
        ui->treeView->expand(m_filterProxy->index(0,0));
}

/*!
 * grabs item activation from treeView and gets the correct
 * model index from proxy.
 *
 * emits itemActivated
 */
void IconListArea::onItemActivation(const QModelIndex & index)
{
    QAction * action = findChild<QAction*>( "clipboardAction" ) ;
    if ( index.data(Qt::UserRole).toInt() == IconListModel::Icon )
      action->setEnabled( true );
    else
      action->setEnabled( false );
      
    emit itemActivated(index);
}

/*!
 * 
 */
void IconListArea::onCopyToClipboard()
{
    QClipboard * clipboard = QApplication::clipboard() ;
    const QModelIndex index = ui->treeView->currentIndex() ;
    const QString id = index.data(Qt::DisplayRole).toString() ;
    clipboard->setText( id );
}

/*!
 * 
 */
void IconListArea::updateStatusTip()
{
    QString status = tr( "%n icon(s) available" , "" , m_filterProxy->iconsCount() );
    emit statusUpdated( status );
}

/*!
 * Force repaint of the treeView.
 * Slot connected to the ThemeSelector::themeUpdated() signal
 */
void IconListArea::refreshView()
{
   for ( int r = 0 ; r < ui->treeView->model()->rowCount() ; r++ )
   {
      const QModelIndex index = ui->treeView->model()->index(r,0) ;
      if (index.isValid())
      {
	 for ( int c = 0 ; c < ui->treeView->model()->rowCount(index) ; c++ )
	 {
	     const QModelIndex childIndex = ui->treeView->model()->index(c,0,index) ;
	     ui->treeView->update(childIndex);
	 }
	 ui->treeView->update(index) ;
      }
   }
}

