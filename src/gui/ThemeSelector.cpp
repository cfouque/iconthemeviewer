/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ThemeSelector.hpp"
#include "ui_ThemeSelector.h"

#include <QIcon>
#include <QDir>
#include <QDebug>

/*!
 * Return the QDir corresponding to the theme
 */
QDir ThemeSelector::getThemeDirectory(const QString& themeName)
{
   const QString tN = ( themeName.isEmpty() ) ? QIcon::themeName() : themeName ;
   
   // find current icons directory
   foreach( QString path , QIcon::themeSearchPaths() )
   {
      QDir dir(path);
      if ( dir.exists(tN) )
      {
	dir.cd( QIcon::themeName() ) ;
	return dir ;
      }
   }
   return QDir() ;
}


/*!
 * Constructor
 */
ThemeSelector::ThemeSelector(QWidget * parent) :
    QWidget(parent)
{
    ui = new Ui::ThemeSelector;
    ui->setupUi(this);
    
    connect( ui->reloadButton , SIGNAL(clicked(bool)) , SLOT(reload()) ) ;
    connect( this , SIGNAL(themeChanged()) , SLOT(repaint()) );
    
    reload();
}

/*!
 * Destructor
 */
ThemeSelector::~ThemeSelector()
{
    delete ui;
}

/*!
 * Populates the theme selector according to the themeSearchPaths.
 * Cursor theme are filtered.
 */
void ThemeSelector::reload()
{
    ui->selector->clear();
    const QString defaultTheme = QIcon::themeName() ;

    foreach (QString folder, QIcon::themeSearchPaths())
    {      
        QDir dir(folder);

        foreach (QString f , dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot))
        {
	    QDir thDir( dir.absoluteFilePath(f) ) ;
	    if ( thDir.exists( "index.theme" ) && !thDir.exists( "cursors" ) )
	       ui->selector->addItem(f);
        }
    }

    int n = ui->selector->findText(defaultTheme) ;
    ui->selector->setCurrentIndex(n);
}

/*!
 * Updates current icon theme and call repaint.
 */
void ThemeSelector::on_selector_currentIndexChanged(QString txt)
{
    QIcon::setThemeName(txt) ;
    emit themeChanged();
}

