/*
 *
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "IconListModel.hpp"
#include "IconListFactory.hpp"
#include <QDebug>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QCloseEvent>

/*!
 * Default constructor
 */
MainWindow::MainWindow() :
    QMainWindow()
{
    ui = new Ui::MainWindow ;
    ui->setupUi(this) ;

#ifndef Q_OS_LINUX
    ui->themeSelector->setVisible(false);
#endif

    IconListFactory * factory = new IconListFactory(this) ;
    factory->createModel(this) ; 
    factory->addQtStandardPixmaps();
#ifdef Q_OS_LINUX
    factory->populateModelFromFile( ":/naming-specs/latest" );
    factory->fetchExtraIcon();
#endif
    ui->listView->setModel(factory->model());

    connect( ui->listView , SIGNAL(itemActivated(QModelIndex)) , ui->iconView , SLOT(setActiveItem(QModelIndex))) ;
    connect( ui->themeSelector , SIGNAL(themeChanged()) , ui->listView , SLOT(refreshView()) ) ;
    connect( ui->themeSelector , SIGNAL(themeChanged()) , ui->iconView , SLOT(refreshView()) ) ;
    
    QLabel * statusLabel = new QLabel(this) ;
    statusBar()->addPermanentWidget( statusLabel );
    connect( ui->listView , SIGNAL(statusUpdated(QString)) , statusLabel , SLOT(setText(QString)) ) ;
    
    readSettings() ;
}

/*!
 * App destructor
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/*!
 * Show Extra icons
 */
void MainWindow::showExtraIcons()
{
    IconListFactory * factory = findChild<IconListFactory*>() ;
    if ( factory )
      factory->fetchExtraIcon();
    else 
       qCritical() << "Unable to retrieve factory" ;
}

/*!
 * Read persistent settings ;
 */
void MainWindow::readSettings()
{
    QSettings settings(qAppName() , qAppName()) ;
    restoreGeometry(settings.value("main/geometry").toByteArray());
    restoreState(settings.value("main/state").toByteArray());
}

/*!
 * Save persistent settings
 */
void MainWindow::writeSettings() const
{
    QSettings settings(qAppName() , qAppName()) ;
    settings.setValue("main/geometry", saveGeometry());
    settings.setValue("main/state", saveState());
};

/*!
 * Close event 
 */
void MainWindow::closeEvent(QCloseEvent * event)
{
    writeSettings(); 
    QWidget::closeEvent(event);
}
