#include "ReadOnlyCheckBox.h"

ReadOnlyCheckBox::ReadOnlyCheckBox(QWidget* parent) : QCheckBox(parent)
{
}

void ReadOnlyCheckBox::keyPressEvent(QKeyEvent* e)
{
}

void ReadOnlyCheckBox::keyReleaseEvent(QKeyEvent* e)
{
}

void ReadOnlyCheckBox::mousePressEvent(QMouseEvent* e)
{
}

void ReadOnlyCheckBox::mouseReleaseEvent(QMouseEvent* e)
{
}

void ReadOnlyCheckBox::mouseDoubleClickEvent(QMouseEvent* )
{
}

//#include "ReadOnlyCheckBox.moc"
