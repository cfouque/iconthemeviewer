/*
 *
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __THEMESELECTOR_HPP__
#define __THEMESELECTOR_HPP__

#include <QWidget>
#include <QDir>

namespace Ui
{
class ThemeSelector;
}

class ThemeSelector : public QWidget
{
    Q_OBJECT

    Ui::ThemeSelector * ui;

public:
    static QDir getThemeDirectory( const QString& themeName = QString() )  ;
    
    explicit ThemeSelector(QWidget * parent = 0);
    ~ThemeSelector();

public slots :
    void on_selector_currentIndexChanged(QString txt) ;
    void reload() ;

signals :
    void themeChanged() ;
    
};

#endif
