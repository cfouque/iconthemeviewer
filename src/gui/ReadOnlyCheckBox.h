#ifndef READONLYCHECKBOX_H
#define READONLYCHECKBOX_H

#include <QCheckBox>

class ReadOnlyCheckBox : public QCheckBox
{
    Q_OBJECT
public:
  explicit ReadOnlyCheckBox(QWidget* parent = 0);

protected:
  virtual void keyPressEvent(QKeyEvent* e);
  virtual void keyReleaseEvent(QKeyEvent* e);
  virtual void mousePressEvent(QMouseEvent* e);
  virtual void mouseReleaseEvent(QMouseEvent* e);
  virtual void mouseDoubleClickEvent(QMouseEvent* );

private:
};

//#include <moc_ReadOnlyCheckBox.cpp>

#endif // READONLYCHECKBOX_H
