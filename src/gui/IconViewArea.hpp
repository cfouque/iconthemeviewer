/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __ICONVIEWAREA_HPP__
#define __ICONVIEWAREA_HPP__

#include <QWidget>
#include <QIcon>
#include <QModelIndex>

class IconListModel;
class QLabel ;
class QCheckBox ;
class ReadOnlyCheckBox;

namespace Ui
{
class IconViewArea;
}

class IconViewArea : public QWidget
{
    Q_OBJECT
    Q_PROPERTY( QSize previewSize READ previewSize WRITE setPreviewSize)
    Q_PROPERTY( QModelIndex activeItem READ activeItem WRITE setActiveItem NOTIFY activeItemUpdated ) 

    Ui::IconViewArea * ui ;
    QMap<QIcon::Mode,QLabel *> m_iconFrames ;
    QMap<int,ReadOnlyCheckBox *> m_sizeBox ;
    QSize m_previewSize;
    QModelIndex m_activeItem ;

public:
    explicit IconViewArea(QWidget * parent);
    ~IconViewArea();
    QSize previewSize() const ;
    QModelIndex activeItem() const ;

public slots :
    void setActiveItem(const QModelIndex & index) ;
    void setPreviewSize(const QSize & size) ;
    void refreshView() ;

private :
    void resetUi() ;
    
signals :
    void activeItemUpdated( const QModelIndex & ) ;
    

};

#endif // ICONVIEWAREA_H
