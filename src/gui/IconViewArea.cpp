/*
 * Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IconViewArea.hpp"
#include "ui_IconViewArea.h"

#include <QResizeEvent>
#include <QDebug>
#include "IconListModel.hpp"


/*!
 * constructor
 */
IconViewArea::IconViewArea(QWidget * parent) :
    QWidget(parent) ,
    m_previewSize(64,64)
{
    ui = new Ui::IconViewArea;
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);

    m_iconFrames.insert(QIcon::Normal , ui->normalFrame) ;
    m_iconFrames.insert(QIcon::Active , ui->activeFrame) ;
    m_iconFrames.insert(QIcon::Disabled , ui->disabledFrame) ;
    m_iconFrames.insert(QIcon::Selected , ui->selectedFrame) ;

    m_sizeBox.insert(8,ui->size8) ;
    m_sizeBox.insert(16,ui->size16) ;
    m_sizeBox.insert(22,ui->size22) ;
    m_sizeBox.insert(32,ui->size32) ;
    m_sizeBox.insert(48,ui->size48) ;
    m_sizeBox.insert(64,ui->size64) ;
    m_sizeBox.insert(128,ui->size128) ;
    m_sizeBox.insert(256,ui->size256) ;
    resetUi(); 
    
    connect( this , SIGNAL(activeItemUpdated(QModelIndex)) , SLOT(refreshView()) ) ;
}

/*!
 * destructor
 */
IconViewArea::~IconViewArea()
{
    delete ui;
}

/*!
 *
 */
QSize IconViewArea::previewSize() const
{
    return m_previewSize ;
}

/*!
 *
 */
void IconViewArea::setPreviewSize(const QSize & size)
{
    m_previewSize = size ;
}

/*!
 * Accessor for current item index
 */
QModelIndex IconViewArea::activeItem() const
{
   return m_activeItem ;
}


/*!
 * Activate display of selected item
 */
void IconViewArea::setActiveItem(const QModelIndex & index)
{
    if (index.isValid())
    {
       m_activeItem = index ;
       emit activeItemUpdated( m_activeItem );
    }
}

/*!
 *
 */
void IconViewArea::resetUi()
{
    foreach (QLabel * lbl , m_iconFrames)
    {
        lbl->clear(); 
        lbl->setFixedHeight( previewSize().height() );
	lbl->setMinimumWidth( previewSize().width() );
    }

    foreach (QCheckBox * cb , m_sizeBox)
    {
        cb->setChecked(false);
    }

}

/*!
 * 
 */
void IconViewArea::refreshView()
{
    int n = activeItem().data(Qt::UserRole).toInt() ;
    IconListModel::ItemType type = static_cast<IconListModel::ItemType>(n) ;
    ui->stackedWidget->setCurrentIndex(n);

    QString id = activeItem().data(Qt::DisplayRole).toString() ;
    QString desc = activeItem().data(Qt::WhatsThisRole).toString() ;

    switch (type)
    {

    case (IconListModel::Category) :
    {
        ui->categoryTitleLabel->setText(id);
        ui->categoryDesc->setText(desc);
        break ;
    }

    case (IconListModel::Icon) :
    {
        resetUi();
        ui->iconTitleLabel->setText(id) ;
        ui->iconDesc->setText(desc);
        QIcon icon = activeItem().data( Qt::DecorationRole ).value<QIcon>() ;
        
        if ( !icon.isNull() )
        {
            foreach ( QIcon::Mode mode , m_iconFrames.keys() )
	    {
	       const QPixmap ipx = icon.pixmap( previewSize() , mode ).scaled( previewSize() , Qt::KeepAspectRatio ) ;
                m_iconFrames[mode]->setPixmap( ipx ) ;
	    }
            
            foreach (QSize s , icon.availableSizes())
            {
                if ( m_sizeBox.contains(s.width()) )
                    m_sizeBox[s.width()]->setChecked(true);
            }
        }
        break ;
    }

    default :
        break ;

    } ;
}
