#include <QApplication>
#include <QtXml>
#include <QStyle>

QString standardPixmapAsString(const QStyle::StandardPixmap& sp)
{
    switch( sp )
    {
    case ( QStyle::SP_TitleBarMinButton ) :
        return "QStyle::SP_TitleBarMinButton" ;
    case ( QStyle::SP_TitleBarMenuButton ) :
        return "QStyle::SP_TitleBarMenuButton" ;
    case ( QStyle::SP_TitleBarMaxButton ) :
        return "QStyle::SP_TitleBarMaxButton" ;
    case ( QStyle::SP_TitleBarCloseButton ) :
        return "QStyle::SP_TitleBarCloseButton" ;
    case ( QStyle::SP_TitleBarNormalButton ) :
        return "QStyle::SP_TitleBarNormalButton" ;
    case ( QStyle::SP_TitleBarShadeButton ) :
        return "QStyle::SP_TitleBarShadeButton" ;
    case ( QStyle::SP_TitleBarUnshadeButton ) :
        return "QStyle::SP_TitleBarUnshadeButton" ;
    case ( QStyle::SP_TitleBarContextHelpButton ) :
        return "QStyle::SP_TitleBarContextHelpButton" ;
    case ( QStyle::SP_MessageBoxInformation ) :
        return "QStyle::SP_MessageBoxInformation";
    case ( QStyle::SP_MessageBoxWarning  ) :
        return "QStyle::SP_MessageBoxWarning";
    case ( QStyle::SP_MessageBoxCritical  ) :
        return "QStyle::SP_MessageBoxCritical";
    case ( QStyle::SP_MessageBoxQuestion  ) :
        return "QStyle::SP_MessageBoxQuestion";
    case ( QStyle::SP_DesktopIcon  ) :
        return "QStyle::SP_DesktopIcon";
    case ( QStyle::SP_TrashIcon  ) :
        return "QStyle::SP_TrashIcon";
    case ( QStyle::SP_ComputerIcon ) :
        return "QStyle::SP_ComputerIcon";
    case ( QStyle::SP_DriveFDIcon  ) :
        return "QStyle::SP_DriveFDIcon";
    case ( QStyle::SP_DriveHDIcon ) :
        return "QStyle::SP_DriveHDIcon";
    case ( QStyle::SP_DriveCDIcon ) :
        return "QStyle::SP_DriveCDIcon";
    case ( QStyle::SP_DriveDVDIcon ) :
        return "QStyle::SP_DriveDVDIcon";
    case ( QStyle::SP_DriveNetIcon  ) :
        return "QStyle::SP_DriveNetIcon";
    case ( QStyle::SP_DirHomeIcon  ) :
        return "QStyle::SP_DirHomeIcon";
    case ( QStyle::SP_DirOpenIcon ) :
        return "QStyle::SP_DirOpenIcon";
    case ( QStyle::SP_DirClosedIcon ) :
        return "QStyle::SP_DirClosedIcon";
    case ( QStyle::SP_DirIcon  ) :
        return "QStyle::SP_DirIcon";
    case ( QStyle::SP_DirLinkIcon ) :
        return "QStyle::SP_DirLinkIcon";
    case ( QStyle::SP_FileIcon ) :
        return "QStyle::SP_FileIcon";
    case ( QStyle::SP_FileLinkIcon  ) :
        return "QStyle::SP_FileLinkIcon";
    case ( QStyle::SP_FileDialogStart ) :
        return "QStyle::SP_FileDialogStart";
    case ( QStyle::SP_FileDialogEnd  ) :
        return "QStyle::SP_FileDialogEnd";
    case ( QStyle::SP_FileDialogToParent ) :
        return "QStyle::SP_FileDialogToParent";
    case ( QStyle::SP_FileDialogNewFolder  ) :
        return "QStyle::SP_FileDialogNewFolder";
    case ( QStyle::SP_FileDialogDetailedView   ) :
        return "QStyle::SP_FileDialogDetailedView";
    case ( QStyle::SP_FileDialogInfoView  ) :
        return "QStyle::SP_FileDialogInfoView";
    case ( QStyle::SP_FileDialogContentsView  ) :
        return "QStyle::SP_FileDialogContentsView";
    case ( QStyle::SP_FileDialogListView  ) :
        return "QStyle::SP_FileDialogListView";
    case ( QStyle::SP_FileDialogBack ) :
        return "QStyle::SP_FileDialogBack";
    case ( QStyle::SP_DockWidgetCloseButton  ) :
        return "QStyle::SP_DockWidgetCloseButton";
    case ( QStyle::SP_ToolBarHorizontalExtensionButton ) :
        return "QStyle::SP_ToolBarHorizontalExtensionButton";
    case ( QStyle::SP_ToolBarVerticalExtensionButton  ) :
        return "QStyle::SP_ToolBarVerticalExtensionButton";
    case ( QStyle::SP_DialogOkButton  ) :
        return "QStyle::SP_DialogOkButton";
    case ( QStyle::SP_DialogCancelButton  ) :
        return "QStyle::SP_DialogCancelButton";
    case ( QStyle::SP_DialogHelpButton ) :
        return "QStyle::SP_DialogHelpButton";
    case ( QStyle::SP_DialogOpenButton  ) :
        return "QStyle::SP_DialogOpenButton";
    case ( QStyle::SP_DialogSaveButton  ) :
        return "QStyle::SP_DialogSaveButton";
    case ( QStyle::SP_DialogCloseButton  ) :
        return "QStyle::SP_DialogCloseButton";
    case ( QStyle::SP_DialogApplyButton  ) :
        return "QStyle::SP_DialogApplyButton";
    case ( QStyle::SP_DialogResetButton ) :
        return "QStyle::SP_DialogResetButton";
    case ( QStyle::SP_DialogDiscardButton ) :
        return "QStyle::SP_DialogDiscardButton";
    case ( QStyle::SP_DialogYesButton  ) :
        return "QStyle::SP_DialogYesButton";
    case ( QStyle::SP_DialogNoButton  ) :
        return "QStyle::SP_DialogNoButton";
    case ( QStyle::SP_ArrowUp  ) :
        return "QStyle::SP_ArrowUp";
    case ( QStyle::SP_ArrowDown  ) :
        return "QStyle::SP_ArrowDown";
    case ( QStyle::SP_ArrowLeft   ) :
        return "QStyle::SP_ArrowLeft";
    case ( QStyle::SP_ArrowRight  ) :
        return "QStyle::SP_ArrowRight";
    case ( QStyle::SP_ArrowBack  ) :
        return "QStyle::SP_ArrowBack";
    case ( QStyle::SP_ArrowForward ) :
        return "QStyle::SP_ArrowForward";
    case ( QStyle::SP_CommandLink ) :
        return "QStyle::SP_CommandLink";
    case ( QStyle::SP_VistaShield ) :
        return "QStyle::SP_VistaShield";
    case ( QStyle::SP_BrowserReload   ) :
        return "QStyle::SP_BrowserReload";
    case ( QStyle::SP_BrowserStop ) :
        return "QStyle::SP_BrowserStop";
    case ( QStyle::SP_MediaPlay ) :
        return "QStyle::SP_MediaPlay";
    case ( QStyle::SP_MediaStop ) :
        return "QStyle::SP_MediaStop";
    case ( QStyle::SP_MediaPause ) :
        return "QStyle::SP_MediaPause";
    case ( QStyle::SP_MediaSkipForward  ) :
        return "QStyle::SP_MediaSkipForward";
    case ( QStyle::SP_MediaSkipBackward ) :
        return "QStyle::SP_MediaSkipBackward";
    case ( QStyle::SP_MediaSeekForward  ) :
        return "QStyle::SP_MediaSeekForward";
    case ( QStyle::SP_MediaSeekBackward   ) :
        return "QStyle::SP_MediaSeekBackward";
    case ( QStyle::SP_MediaVolume  ) :
        return "QStyle::SP_MediaVolume";
    case ( QStyle::SP_MediaVolumeMuted   ) :
        return "QStyle::SP_MediaVolumeMuted";
    case ( QStyle::SP_CustomBase ) :
        return "QStyle::SP_CustomBase";
    default :
        break;
    }
    return QString() ;

}

int main( int argc , char * argv[] ) 
{
        
    QDomDocument doc( "qt_standard_pixmap" ) ;
    
    QDomElement rootElt = doc.createElement( "enum" ) ;
    rootElt.setAttribute( "id" , "QStyle::StandardPixmap" ) ;
    doc.appendChild(rootElt) ;
    
    QDomElement rootDescElt = doc.createElement( "desc" ) ;
    rootElt.appendChild( rootDescElt ) ;
    QDomText rootDescText = doc.createTextNode( "This category contains the standard pixmap provided by the Qt enum QStyle::StandardPixmap." ) ;
    rootDescElt.appendChild( rootDescText ) ;
    
    for( int n = 0 ; n < 69 ; n++ ) 
    {
        QStyle::StandardPixmap sp = static_cast<QStyle::StandardPixmap>(n) ;
        qDebug() << sp << standardPixmapAsString(sp) ;
        
        QDomElement valueElt = doc.createElement( "value" ) ;
        valueElt.setAttribute( "id" , n );
        
        QDomElement nameElt = doc.createElement( "name" );
        valueElt.appendChild(nameElt) ;
        QDomText nameText = doc.createTextNode( QString(standardPixmapAsString(sp) ) ) ;
        nameElt.appendChild(nameText) ;
        
        QDomElement descElt = doc.createElement( "desc" ) ;
        QDomText descText = doc.createTextNode( QString() ) ;
        descElt.appendChild(descText) ;
        valueElt.appendChild( descElt ) ;
        
        rootElt.appendChild(valueElt) ;
    }
        
    QFile file( "qt_standard_pixmap.xml" );
    if ( !file.open( QIODevice::ReadWrite | QIODevice::Text ) )
    {
        qCritical() << "Unable to open" << file.fileName() << file.errorString() ;
        return 0 ;
    }
    else 
    {
        QTextStream stream(&file) ;
        stream << doc.toString() ;
        file.close(); 
    }
 
    return 0 ;
} ;