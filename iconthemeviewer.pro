# Copyright (C) 2014  Clément Fouque <clement.fouque@free.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iconthemeviewer
TEMPLATE = app

INCLUDEPATH += src/model src/gui

HEADERS += \
    src/gui/IconListArea.hpp \
    src/gui/IconViewArea.hpp \
    src/gui/MainWindow.hpp \
    src/gui/ReadOnlyCheckBox.h \
    src/gui/ThemeSelector.hpp \
    src/model/IconListFactory.hpp \
    src/model/IconListModel.hpp \
    src/model/TreeFilterProxyModel.hpp

SOURCES += \
    src/gui/IconListArea.cpp \
    src/gui/IconViewArea.cpp \
    src/gui/MainWindow.cpp \
    src/gui/ReadOnlyCheckBox.cpp \
    src/gui/ThemeSelector.cpp \
    src/model/IconListFactory.cpp \
    src/model/IconListModel.cpp \
    src/model/TreeFilterProxyModel.cpp \
    src/main.cpp

FORMS += \
    src/gui/IconListArea.ui \
    src/gui/IconViewArea.ui \
    src/gui/MainWindow.ui \
    src/gui/ThemeSelector.ui

RESOURCES += \
    resources/IconThemeViewer.qrc

